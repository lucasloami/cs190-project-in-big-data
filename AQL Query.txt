create dataverse feeds;
use dataverse feeds;
create type TwitterUser as open {
      screen_name: string,
      lang: string,
      friends_count: int32,
      statuses_count: int32,
      name: string,
      followers_count: int32
} 

create type Tweet as open {
      tweetid: string,
      user: TwitterUser,
      location_lat: double,
      location_long: double,
      send_time: string,
      message_text: string
}
create type ProcessedTweet as open {
      tweetid: string,
      user: TwitterUser,
      location_lat: double,
      location_long: double,
      send_time: string,
      message_text: string,
      polarity: string,
      formattedDate: string
}
create dataset Tweets(ProcessedTweet)
primary key tweetid;






managix install -n my_asterix -d feeds -l sentimentlib -p /home/terence/project-library.zip 





use dataverse feeds;
create feed TwitterFeed using
pull_twitter (("query"="Obama"))
apply function sentimentlib#SentimentAnalysis;

connect feed TwitterFeed to dataset Tweets;






use dataverse feeds;

for $x in dataset Tweets
return $x






use dataverse feeds;

for $t in dataset Tweets
where $t.user.lang = "en"
and get-date-from-datetime(datetime($t.formattedDate)) = date("2014-06-12")
let $polarity := $t.polarity
group by $hour := get-hour(datetime($t.formattedDate)), $date := get-date-from-datetime(datetime($t.formattedDate)) with $polarity
return {
"hour" : $hour,
"date" : $date,
"negative": count(for $p in $polarity 
                    where $p = "negative"
                    return $p),
"positive": count(for $p in $polarity 
                    where $p = "positive"
                    return $p),
"neutral": count(for $p in $polarity 
                    where $p = "neutral"
                    return $p)
}
