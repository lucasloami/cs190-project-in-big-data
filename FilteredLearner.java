package sentimentanalysis;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectOutputStream;


//import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.bayes.NaiveBayesMultinomial;
import weka.classifiers.meta.FilteredClassifier;
import weka.core.Instances;
import weka.core.converters.ArffLoader.ArffReader;
import weka.core.tokenizers.WordTokenizer;
import weka.filters.unsupervised.attribute.StringToWordVector;

/**
 * This code is based on the code described in the following post:
 * http://jmgomezhidalgo.blogspot.com/2013/04/a-simple-text-classifier-in-java-with.html
 * 
 * This class is responsible for train a Naive Bayes classifier with
 * a binary polarity dataset
 * 
 * TODO check if POS Tags are viable for sentiment analysis features in this project
 * 
 * @author lucas
 *
 */


public class FilteredLearner {
	private Instances trainData;
	private StringToWordVector filter;
	private FilteredClassifier classifier;
	
	/**
	* This method loads a dataset in ARFF format. If the file does not exist, or
	* it has a wrong format, the attribute trainData is null.
	* @param fileName The name of the file that stores the dataset.
	*/
	public void loadDataset(String fileName) {
		try {
			BufferedReader reader = new BufferedReader(new FileReader(fileName));
			ArffReader arff = new ArffReader(reader);
			trainData = arff.getData();
			System.out.println("===== Loaded dataset: " + fileName + " =====");
			reader.close();
		}
		catch (IOException e) {
			e.printStackTrace();
			System.out.println("Problem found when reading: " + fileName);
		}
	}
	
	/**
	* This method evaluates the classifier. As recommended by WEKA documentation,
	* the classifier is defined but not trained yet. Evaluation of previously
	* trained classifiers can lead to unexpected results.
	*/
	/*public void evaluate() {
		try {
			trainData.setClassIndex(0);
			filter = new StringToWordVector();
			filter.setAttributeIndices("last");
			classifier = new FilteredClassifier();
			classifier.setFilter(filter);
			classifier.setClassifier(new NaiveBayes());
			Evaluation eval = new Evaluation(trainData);
			eval.crossValidateModel(classifier, trainData, 4, new Random(1));
			System.out.println(eval.toSummaryString());
			System.out.println(eval.toClassDetailsString());
			System.out.println("===== Evaluating on filtered (training) dataset done =====");
		}
		catch (Exception e) {
			System.out.println("Problem found when evaluating");
		}
	}*/
	
	/**
	* This method trains the classifier on the loaded dataset.
	*/
	public void learn() {
		try {
			//set the class attribute in the trainData
			trainData.setClassIndex(0);
			
			//TODO implement POS Tags for the features
			
			filter = new StringToWordVector();
			filter.setAttributeIndices("first-last");
			filter.setUseStoplist(true);
			filter.setLowerCaseTokens(true);
			filter.setTokenizer(new WordTokenizer());
			classifier = new FilteredClassifier();
			classifier.setFilter(filter);
			classifier.setClassifier(new NaiveBayesMultinomial());
			classifier.buildClassifier(trainData);
			// Uncomment to see the classifier
			System.out.println(classifier);
			System.out.println("===== Training on filtered (training) dataset done =====");
		}
		catch (Exception e) {
			e.printStackTrace();
			System.out.println("Problem found when training");
		}
	}
	
	/**
	* This method saves the trained model into a file. This is done by
	* simple serialization of the classifier object.
	* @param fileName The name of the file that will store the trained model.
	*/
	public void saveModel(String fileName) {
		try {
			ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(fileName));
		    out.writeObject(classifier);
		    out.close();
		    System.out.println("===== Saved model: " + fileName + " =====");
		}
		catch (IOException e) {
			System.out.println("Problem found when writing: " + fileName);
		}
	}
	
	public static void main (String[] args) {

		FilteredLearner learner;
		System.out.println(args.length);
		if (args.length < 2)
			System.out.println("Usage: java MyLearner <fileTrainData> <fileTrainedModel>");
		else {
			learner = new FilteredLearner();
			learner.loadDataset(args[0]);
			// Evaluation must be done before training
			// More info in: http://weka.wikispaces.com/Use+WEKA+in+your+Java+code
			//learner.evaluate();
			learner.learn();
			learner.saveModel(args[1]);
		}
	}
}
