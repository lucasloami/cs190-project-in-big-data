package sentimentanalysis;
/**
 * A Java class that implements a simple text classifier, based on WEKA.
 * To be used with MyFilteredLearner.java.
 * WEKA is available at: http://www.cs.waikato.ac.nz/ml/weka/
 * Copyright (C) 2013 Jose Maria Gomez Hidalgo - http://www.esp.uem.es/jmgomez
 *
 * This program is free software: you can redistribute it and/or modify
 * it for any purpose.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;

//import weka.classifiers.bayes.NaiveBayesMultinomial;
import weka.classifiers.meta.FilteredClassifier;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;

/**
 * This class implements a simple text classifier in Java using WEKA.
 * It loads a file with the text to classify, and the model that has been
 * learnt with MyFilteredLearner.java.
 * @author Jose Maria Gomez Hidalgo - http://www.esp.uem.es/jmgomez
 * @see MyFilteredLearner
 */
public class MyFilteredClassifier {

	/**
	 * String that stores the text to classify
	 */
	private String text;
	/**
	 * Object that stores the instance.
	 */
	private Instances instances;
	/**
	 * Object that stores the classifier.
	 */
	private FilteredClassifier classifier = null;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}


	/**
	 * This method loads the text to be classified.
	 * @param fileName The name of the file that stores the text.
	 */
	public void load(String fileName) {
		try {
			BufferedReader reader = new BufferedReader(new FileReader(fileName));
			String line;
			text = "";
			while ((line = reader.readLine()) != null) {
				text = text + " " + line;
			}
			System.out.println("===== Loaded text data: " + fileName + " =====");
			reader.close();
			System.out.println(text);
		}
		catch (IOException e) {
			System.out.println("Problem found when reading: " + fileName);
		}
	}

	/**
	 * This method loads the model to be used as classifier.
	 * @param fileName The name of the file that stores the text.
	 */
	public void loadModel(String fileName) {
		try {
			ObjectInputStream in = new ObjectInputStream(new FileInputStream(fileName));
			Object tmp = in.readObject();
			classifier = (FilteredClassifier) tmp;
			in.close();
			System.out.println("===== Loaded model: " + fileName + " =====");
		}
		catch (Exception e) {
			// Given the cast, a ClassNotFoundException must be caught along with the IOException
			System.out.println(new File(MyFilteredClassifier.class.getProtectionDomain().getCodeSource().getLocation().getPath()));
			System.out.println("Problem found when reading: " + fileName);
		}
	}

	/**
	 * This method creates the instance to be classified, from the text that has been read.
	 */
	public void makeInstance() {
		// Create the attributes, class and text
//		FastVector fvNominalVal = new FastVector(3);
		FastVector fvNominalVal = new FastVector(2);
		fvNominalVal.addElement("negative");
		fvNominalVal.addElement("positive");
//		fvNominalVal.addElement("neutral");
		Attribute attribute1 = new Attribute("polarity", fvNominalVal);
		Attribute attribute2 = new Attribute("tweet",(FastVector) null);
		// Create list of instances with one element
		FastVector fvWekaAttributes = new FastVector(2);
		fvWekaAttributes.addElement(attribute2);
		fvWekaAttributes.addElement(attribute1);
		instances = new Instances("Test relation", fvWekaAttributes, 1);
		// Set class index
		instances.setClassIndex(1);
		// Create and add the instance
		Instance instance = new Instance(2);
		text = "sad fuck shit unhappy";
		instance.setValue(attribute2, text);
		// Another way to do it: (set to elementAt(0))
//		instance.setValue((Attribute)fvWekaAttributes.elementAt(1), text);
		instances.add(instance);
		System.out.println("===== Instance created with reference dataset =====");
		System.out.println(instances);
	}

	/**
	 * This method performs the classification of the instance.
	 * Output is done at the command-line.
	 * @return 
	 */
	public String classify() {
		try {
			double pred = classifier.classifyInstance(instances.instance(0));
//			System.out.println("===== Classified instance =====");
			System.out.println("Class predicted: " + instances.classAttribute().value((int) pred));
			
			return instances.classAttribute().value((int) pred);
		}
		catch (Exception e) {
			e.printStackTrace();
			System.out.println("Problem found when classifying the text");
		}
		return "";
	}

	/**
	 * Main method. It is an example of the usage of this class.
	 * @param args Command-line arguments: fileData and fileModel.
	 */
	public static void main (String[] args) {

		MyFilteredClassifier classifier;
		if (args.length < 2)
			System.out.println("Usage: java MyClassifier <fileData> <fileModel>");
		else {
			classifier = new MyFilteredClassifier();
			classifier.load(args[0]);
			classifier.loadModel(args[1]);
			classifier.makeInstance();
			classifier.classify();
		}
	}
}	
