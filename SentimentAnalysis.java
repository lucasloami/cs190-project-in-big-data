package sentimentanalysis;
import edu.uci.ics.asterix.external.library.IExternalScalarFunction;
import edu.uci.ics.asterix.external.library.IFunctionHelper;
import edu.uci.ics.asterix.external.library.java.JObjects.JRecord;
import edu.uci.ics.asterix.external.library.java.JObjects.JString;


public class SentimentAnalysis implements IExternalScalarFunction {

	private MyFilteredClassifier classifier;
	
	@Override
	public void deinitialize() {
		
	}

	@Override
	public void initialize(IFunctionHelper arg0) throws Exception {
		classifier = new MyFilteredClassifier();
//		classifier.loadModel("/home/terence/weka-sentiment-analysis/classifiers/naive-bayes.dat");
		classifier.loadModel("/home/terence/weka-sentiment-analysis/classifiers/nbmultinomial.dat");
	}

	@Override
	public void evaluate(IFunctionHelper arg0) throws Exception {
		JRecord inputRecord = (JRecord) arg0.getArgument(0);
		JString text = (JString) inputRecord.getValueByName("message_text");
		
		System.out.println("text value:" + text.getValue());
		classifier.setText(text.getValue());
		classifier.makeInstance();
		String string =  classifier.classify();
		JString polarity  = new JString(string);
		
		JRecord result = (JRecord) arg0.getResultObject();
        result.setField("tweetid", inputRecord.getFields()[0]);
        result.setField("user", inputRecord.getFields()[1]);
        result.setField("location_lat", inputRecord.getFields()[2]);
        result.setField("location_long", inputRecord.getFields()[3]);
        result.setField("send_time", inputRecord.getFields()[4]);
        result.setField("message_text", inputRecord.getFields()[5]);
        result.setField("polarity", polarity);
		
		arg0.setResult(result);
	}
	
}
