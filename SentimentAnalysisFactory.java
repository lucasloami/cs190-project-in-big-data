package sentimentanalysis;
import edu.uci.ics.asterix.external.library.IExternalScalarFunction;
import edu.uci.ics.asterix.external.library.IFunctionFactory;

public class SentimentAnalysisFactory implements IFunctionFactory {

    @Override
    public IExternalScalarFunction getExternalFunction() {
        return new SentimentAnalysis();
    }
}