import requests
from requests import ConnectionError, HTTPError
import sys
import re

class AQLBuilder(object):
    """Used to build the AQL to be performed in AsterixDB"""

    Afor_clause = ""
    Areturn_clause = ""
    Adataverse_clause = ""
    Awhere_clause = ""
    Alimit_clause = ""
    Aquery = ""
    Aorder_by_clause = ""
    Aendpoint = ""

    def __init__(self, dataverse, endpoint):
        #super(ClassName, self).__init__()
        #self.arg = arg
        self.Aendpoint = endpoint
        self.Adataverse_clause = "use dataverse " + dataverse + ";"
        self.Afor_clause = ""
        self.Awhere_clause = ""
        self.Alimit_clause = ""
        self.Areturn_clause = ""
        self.Aquery = ""
        self.Aorder_by_clause = ""

    def for_clause(self, var, dataset):
        self.Afor_clause = "for " + var + " in dataset " + dataset

    def return_clause(self, return_dict):
        self.Areturn_clause = "return " + return_dict + ";"

    #def dataverse_clause(self, dataverse):
    #    dataverse_clause = dataverse_clause

    def where_clause(self, key, value, op="="):
        if (self.Awhere_clause != ""):
            self.Awhere_clause += " and "
        else:
            self.Awhere_clause += "where " 
        self.Awhere_clause += key + op + "\"" + value + "\""

    def limit_clause(self, limit):
        self.Alimit_clause = "limit " + str(limit)

    def order_by_clause(self, key, order):
        self.Aorder_by_clause = "order by " + key + " " + order

    def group_by_clause(self, var, expression, with_var):
        self.Agroup_by_clause = "group by " + var + ":=" + expression + " with " + with_var

    def get_date_from_datetime_func(self, datetime):
        return "get-date-from-datetime(datetime(" + datetime + ")) "

    def get_hour_func(self, value):
        return "get-hour(" + value + ") "
                

    def aql_clause(self, aql):
        self.Aquery = aql

    def result(self):
        self.Aquery += self.Adataverse_clause + "\n"
        self.Aquery += self.Afor_clause + "\n"
        self.Aquery += self.Awhere_clause + "\n"
        self.Aquery += self.Aorder_by_clause + "\n"
        self.Aquery += self.Alimit_clause + "\n"
        self.Aquery += self.Areturn_clause + "\n"

        data = ""

        if (self.Aendpoint == "query"):
            data = self.build_query()
        elif(self.Aendpoint == "ddl" or self.Aendpoint == "update"): 
            data = self.build_statements()
        #TODO query status and query result

    #teste = teste.replace("\"
        #return self.Aquery
        return self.build_response(data)

    def build_response(self, data):
        http_header = { "content-type": "application/json" }

        api_endpoint = "http://localhost:19002/" + self.Aendpoint
        try:
            response = requests.get(api_endpoint, params=data, headers=http_header)
            #print response.json()
            return self.clean_json(response.json())
            #return response.json()
        except (ConnectionError, HTTPError):
            print "Encountered connection error; stopping execution"
            sys.exit(1)

    def build_query(self):
        query = { 
            "query": self.Aquery,
            "mode": "synchronous"
        }
        return query

    def build_statements(self):
        query = { 
            "statements": self.Aquery,
        }
        return query

    def clean_json(self, incorrect_json):
        data = [re.sub(r'\s+', ' ', elem) for elem in incorrect_json['results']]
        data = [elem.replace('int64', '\"int64\"') for elem in data]
        data = [elem.replace('date:', '') for elem in data]
        data = [eval(elem.replace('int32', '\"int32\"')) for elem in data]
        #print data
        #data = data[0] + data[3:len(data)-3] + data[len(data)-1] #removes the unicode identifiers from string
        #print data
        
        return data  