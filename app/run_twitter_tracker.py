# -*- coding: utf-8 -*-

#import admaql101
#import requests
from bottle import route, run, jinja2_template as template, static_file, request, TEMPLATE_PATH
from aql_builder import AQLBuilder
#import re
import json

TEMPLATE_PATH.append('./views')

#http_header = { "content-type": "application/json" }
#debug(True)

# Core Routing
@route('/')
def jsontest():
    return template('index')

@route('/dashboard')
def dashboard():
    aql = AQLBuilder('feeds', 'query')
    aql.aql_clause('''
                    use dataverse feeds;

                    for $t in dataset Tweets
                    where $t.user.lang = "en"
                    and get-date-from-datetime(datetime($t.formattedDate)) = date("2014-06-12")
                    let $polarity := $t.polarity
                    group by $hour := get-hour(datetime($t.formattedDate)), $date := get-date-from-datetime(datetime($t.formattedDate)) with $polarity
                    return {
                    "hour" : $hour,
                    "negative": count(for $p in $polarity 
                                        where $p = "negative"
                                        return $p),
                    "positive": count(for $p in $polarity 
                                        where $p = "positive"
                                        return $p),
                    "neutral": count(for $p in $polarity 
                                        where $p = "neutral"
                                        return $p)
                    }
                ''')


    #results = aql.result()

    sentiment_info = aql.result()
    positive = 0
    negative = 0
    neutral = 0
    line_chart_data = []
    for t in sentiment_info: 
        print t 
        hour_clean = ''
        if t['hour']['int32'] < 10:
            hour_clean = '0' + str(t['hour']['int32'])
        else:
            hour_clean = str(t['hour']['int32'])
        positive += t['positive']['int64']
        negative += t['negative']['int64']
        neutral += t['neutral']['int64']
        line_chart_data.append({
                "hour": "2014-06-12 " + hour_clean + ":00" ,
                "pos": t['positive']['int64'],
                "neg": t['negative']['int64'],
                "neu": t['neutral']['int64']
            })

    donut_chart_data = [
        {"label": "Positive", "value": positive},
        {"label": "Negative", "value": negative},
        {"label": "Neutral", "value": neutral}
    ]

    #line_chart_data = []
    #for result in results:
    #    line_chart_data.append(
    #        {'hour':result['hour']['int32'],
    #         'pos': result['positive']['int64'],
    #         'neg': result['negative']['int64'],
    #         'neu': result['neutral']['int64'],
    #        }
    #    )
    #print line_chart_data

    return template('dashboard_index.html', donut_chart_data=json.dumps(donut_chart_data), line_chart_data=json.dumps(line_chart_data))

@route('/timeline')
def timeline():
    aql = AQLBuilder('feeds', 'query')
    aql.for_clause("$tweets", "Tweets")
    aql.order_by_clause("$tweets.send_time", "desc")
    aql.where_clause("$tweets.user.lang", "en")
    aql.limit_clause(10)
    aql.return_clause("$tweets")
    results = aql.result()

    #print result[0]['tweetid']
    #print type(result['results'])
    #try:
    #    json.loads(str(result['results']))
    #except ValueError:
    #    print "Error to decode string"
    #teste = eval(result['results'][0].replace('int32', '\'int32\''))

    #teste = result['results'][4].replace('int32', '\'int32\'')
    #teste = re.sub('\s+', " ", teste)
    #teste = teste.strip()
    #teste = teste.replace("\"", "\\\"")
    #print teste
    #print eval(teste)

    for t in results:
        t['message_text'] = t['message_text'].decode('utf-8')

    return template('dashboard_timeline', tweets=results)
    #return template('{{tweets}}', tweets=teste)

@route('/new_tweets', method="POST")
def json_new_tweets():
    last_date = request.json['date']

    aql = AQLBuilder('feeds', 'query')
    aql.for_clause("$tweets", "Tweets")
    aql.order_by_clause("$tweets.send_time", "desc")
    aql.where_clause("$tweets.user.lang", "en")
    aql.where_clause("$tweets.send_time", last_date, '<')
    aql.limit_clause(10)
    aql.return_clause("$tweets")
    result = aql.result()

    return json.dumps(result)
    

@route('/static/<filename:path>')
def send_static(filename):
    return static_file(filename, root='static')

# API Helpers
'''def build_response(endpoint, data):
    api_endpoint = "http://localhost:19002/" + endpoint
    response = requests.get(api_endpoint, params=data, headers=http_header)
    try:
        return response.json();
    except ValueError:
        return []

# API Endpoints    
@route('/query')
def run_asterix_query():
    return (build_response("query", dict(request.query)))
    
@route('/query/status')
def run_asterix_query_status():
    return (build_response("query/status", dict(request.query)))

@route('/query/result')
def run_asterix_query_result():
    return (build_response("query/result", dict(request.query)))


@route('/ddl')
def run_asterix_ddl():
    return (build_response("ddl", dict(request.query)))

@route('/update')
def run_asterix_update():
    return (build_response("update", dict(request.query)))
''' 
#res = admaql101.bootstrap()
run(host='localhost', port=8081, debug=True, reloader=True)
