$(document).ready(function(){

	var A = new AsterixDBConnection().dataverse("feeds");

    /*function populateLineChart(res) {
        var data = [];
        for(var i in res.results) {
            var bad_json = res.results[0];
            var good_json = bad_json.replace(/int32/g,"\"int32\""); // 
            good_json = good_json.replace(/\s/g, " "); // replace all whitespace chars for single space
            good_json = $.parseJSON(good_json);

            var item = {};
            item['time'] = new Date(good_json.tweets.send_time);
        }
    }*/


    function populateDonutChart(res) {
        var pos_counter = 0;
        var neg_counter = 0;
        var neu_counter = 0;
        for (var i in res.results) {
            //$(dom).append(res[i] + "<br/>");
            //console.log("entered");
            var bad_json = res.results[0];
            var good_json = bad_json.replace(/int32/g,"\"int32\""); // 
            good_json = good_json.replace(/\s/g, " "); // replace all whitespace chars for single space
            good_json = $.parseJSON(good_json);

            var send_time = good_json.tweets.send_time;
            var date_send_time = new Date(send_time);
            //console.log(good_json.tweets.send_time);
            //console.log((date_send_time.getMonth()+1) +"/"+ date_send_time.getDate() + "/" +date_send_time.getFullYear());
            console.log(send_time);

            if (good_json.tweets.polarity == "positive") {pos_counter += 1;}
            else if (good_json.tweets.polarity == "negative") {neg_counter += 1;}
            else neu_counter += 1;

            if (i == 2) {break;}
        }

        //console.log("positive = " + pos_counter + " negative = " + neg_counter + " neutral " + neu_counter);

        Morris.Donut({
          element: 'morris-donut-chart-tweets',
          data: [
            {label: "positive", value: pos_counter},
            {label: "negative", value: neg_counter},
            {label: "neutral", value: neu_counter}
          ],resize: true
        });
    }


	$("#topic-obama").click(function() {
		var allTweets = new FLWOGRExpression()
            .ForClause("$tweets", new AExpression("dataset Tweets"))
            .ReturnClause({"tweets": "$tweets"});
        
        var success0a = function(res) {
            /*var bad_json = res.results[0];
            var good_json = bad_json.replace(/int32/g,"\"int32\"");
            good_json = good_json.replace(/\s/g, " ");
            good_json = $.parseJSON(good_json);*/
            //populateLineChart(res);
            //populateDonutChart(res);
            console.log(good_json.tweets.send_time);
        };
        
        A.query(allTweets.val(), success0a);

	});

    /*var A = new AsterixDBConnection().dataverse("TinySocial");
    
    // 0A - Exact-Match Lookup
    $('#topic-obama').click(function () {
        $('#result0a').html('');
        
        var expression0a = new FLWOGRExpression()
            .ForClause("$user", new AExpression("dataset FacebookUsers"))
            .WhereClause(new AExpression("$user.id = 8"))
            .ReturnClause("$user");
        
        var success0a = function(res) {
            //addResult('#result0a', res["results"]);
            console.log('Hey, heeeeere');
        };
        
        A.query(expression0a.val(), success0a);
    });*/

});
