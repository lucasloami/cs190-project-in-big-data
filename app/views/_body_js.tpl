<!-- Core Scripts - Include with every page -->
<!-- <script src="static/dashboard/js/jquery-1.10.2.js"></script>
<script src="static/dashboard/js/bootstrap.min.js"></script> -->
<script src="static/dashboard/js/plugins/metisMenu/jquery.metisMenu.js"></script>

<!-- Page-Level Plugin Scripts - Dashboard -->
<script src="static/dashboard/js/plugins/morris/raphael-2.1.0.min.js"></script>
<script src="static/dashboard/js/plugins/morris/morris.js"></script>

<!-- SB Admin Scripts - Include with every page -->
<script src="static/dashboard/js/sb-admin.js"></script>

<!-- Page-Level Demo Scripts - Dashboard - Use for reference -->
<!--<script src="static/dashboard/js/demo/dashboard-demo.js"></script> -->

% for js in js_files:
<script src="{{js}}"></script>
%end