<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>{{title}}</title>

    <!-- Core CSS - Include with every page -->
    <link href="static/dashboard/css/bootstrap.min.css" rel="stylesheet">
    <link href="static/dashboard/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Page-Level Plugin CSS - Dashboard -->
    <link href="static/dashboard/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
    <link href="static/dashboard/css/plugins/timeline/timeline.css" rel="stylesheet">

    <!-- SB Admin CSS - Include with every page -->
    <link href="static/dashboard/css/sb-admin.css" rel="stylesheet">

    <!-- <script src="static/js/jquery.min.js"></script> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- <script src="static/js/bootstrap.min.js"></script> -->
    <script src="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <script src="static/js/asterix-sdk-stable.js"></script>
    <!-- <script src="static/js/demo.js"></script> -->
    <script src="static/js/twitter-tracker.js"></script>
    <!-- <script src="static/js/dashboard-index.js"></script> -->


</head>


<body>