% include('base_header.tpl', title="Dashboard")

    <div id="wrapper">

        % include('_header_navigation.tpl')

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Dashboard</h1>
                    <!-- <button class="btn btn-default" type="button" id="pull-obama-data">
                        Pull data
                    </button> -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-8">
                   <div class="panel panel-default">
                        <div class="panel-heading">
                            Tweets during a day
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div id="tweets-line-chart"></div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-6 -->
                <div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Total polarity distribution
                        </div>
                        <div class="panel-body">
                            <div id="tweets-donut-chart"></div>
                            <!-- <a href="#" class="btn btn-default btn-block">View Details</a> -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
            <div>
                
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

% js_files = []
% #js_files.insert(0, "static/dashboard/js/demo/morris-demo.js")

% include('_body_js.tpl', js_files=js_files)

% include('base_footer.tpl')

<script type="text/javascript">
    var line_chart_data = {{line_chart_data}};

    $(document).ready(function() {
        Morris.Line({
            element: 'tweets-line-chart',
            data: [
                { hour: '2012-02-24 15:00', pos: 100, neg: 90, neu: 97 },
                { hour: '2012-02-24 16:00', pos: 75,  neg: 65, neu: 55 },
                { hour: '2012-02-24 17:00', pos: 50,  neg: 40, neu: 60 },
                { hour: '2012-02-24 18:00', pos: 75,  neg: 65, neu: 85 },
                { hour: '2012-02-24 19:00', pos: 50,  neg: 40, neu: 30 },
                { hour: '2012-02-24 20:00', pos: 75,  neg: 65, neu: 110 },
                { hour: '2012-02-24 21:00', pos: 100, neg: 90, neu: 10 }
            ],
            xkey: 'hour',
            ykeys: ['pos', 'neg', 'neu'],
            labels: ['Positive', 'Negative', 'Neutral']
        });
        Morris.Donut({
            element: 'tweets-donut-chart',
            data: [
                {label: "Positive", value: 60},
                {label: "Negative", value: 30},
                {label: "Neutral", value: 10}
            ],
            formatter: function (y, data) { return y + '%' }
        });
    });

</script>
