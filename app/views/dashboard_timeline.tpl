% include('base_header.tpl', title="Timeline")

    <div id="wrapper">

        % include('_header_navigation.tpl')

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Timeline</h1>
                    <!-- <button class="btn btn-default" type="button" id="pull-obama-data">
                        Pull data
                    </button> -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-clock-o fa-fw"></i> Timeline
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <ul class="timeline">
                                % last_date = ""
                                % for tweet in tweets:
                                        % #import re
                                        % #tweet = re.sub("\s+", " ", tweet)
                                        % #obj = eval(tweet.replace('int32', '\'int32\''))
                                        % last_date = tweet['send_time']
                                        % inverted = ""
                                        % badge_color = ""
                                        % icon = ""
                                        % if (tweet['polarity'] == "negative"):
                                        %    inverted = "timeline-inverted"
                                        %    badge_color = "danger"
                                        %    icon = "fa fa-thumbs-o-down"
                                        % elif (tweet['polarity'] == "positive"):
                                        %    badge_color = "success"
                                        %    icon = "fa fa-thumbs-o-up"
                                        % end
                                    
                                    <li  class={{inverted}} >
                                        <div class="timeline-badge {{badge_color}}"><i class="{{icon}}"></i>
                                        </div>
                                        <div class="timeline-panel">
                                            <div class="timeline-heading">
                                                <h4 class="timeline-title">{{tweet['user']['name']}}</h4>
                                                <p>
                                                    <small class="text-muted"><i class="fa fa-time"></i> {{tweet['send_time']}}</small>
                                                </p>
                                            </div>
                                            <div class="timeline-body">
                                                <p>{{tweet['message_text']}}</p>
                                            </div>
                                        </div>
                                    </li>    
                                % end
                            </ul>
                            <div id="inifiniteLoader-message-div" style="display:none;height:100px;">
                                <p>
                                    <a id="inifiniteLoader" >Loading... <i class="fa fa-spinner fa-spin"></i></a>
                                </p>
                            </div>
                            <input type="hidden" value="{{last_date}}" id="last-date"/>
                            <div class="infiniteLoader-lazy-load" style="display:none;">
                                <p>
                                    <button type="button" class="btn btn-default" id="run-lazy-load">Load more</button>
                                </p>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-8 -->
                <!-- <div class="col-lg-4">

                </div> -->
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

% js_files = ""
% include('_body_js.tpl', js_files=js_files)

    <script type="text/javascript">
        var isMax = false;
        var maxAutoLoad = 3;
        var loadCounter = 0;

        function loadTweets() {
            $('div#inifiniteLoader-message-div').show('fast');
            var last_date = $('#last-date').val();
            ajaxRunning = true;
            $.ajax({
                url: "/new_tweets",
                type:'POST',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({'date': last_date}),
                async: false,
                dataType: "json",
                success: function(data){
                    if (data.length == 0) {
                        isMax = true;
                    }
                    
                    for (var i = 0; i < data.length; i++) {
                        /*var cleaned_data = data.results[i].replace(/\s+/g, " ");
                        cleaned_data = cleaned_data.replace(/int32/g,"\"int32\"" );
                        cleaned_data = $.parseJSON(cleaned_data);*/
                        //console.log(data[i]);
                        var cleaned_data = data[i];
                        var newElem = buildTimelineElement(cleaned_data);
                        $(".timeline").append(newElem);
                        $("#last-date").val(cleaned_data.send_time);
                    };
                    $('a#inifiniteLoader').hide('fast');
                    loadCounter++;
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $('a#inifiniteLoader').hide('fast');
                    alert(textStatus);
                    isMax = true;
                }
            });
            return false;
        };

        function buildTimelineElement(data) {
            var inverted = "";
            var badge_color = "";
            var icon = "";

            if (data.polarity == "negative") {
                inverted = "timeline-inverted";
                badge_color = "danger";
                icon = "fa fa-thumbs-o-down";
            } else if (data.polarity == "positive") {
                badge_color = "success";
                icon = "fa fa-thumbs-o-up";
            }


            var elem = "<li  class="+ inverted + " >" +
            "<div class=\"timeline-badge "+ badge_color + "\"><i class=\"" + icon + "\"></i></div>" +
            "<div class=\"timeline-panel\"> " +
            "<div class=\"timeline-heading\">" +
            "<h4 class=\"timeline-title\">" + data.user.name + "</h4>" +
            "<p><small class=\"text-muted\"><i class=\"fa fa-time\"></i>" + data.send_time + "</small></p>" +
            "</div> " +
            "<div class=\"timeline-body\"> <p>" + data.message_text + "</p></div></div> </li>";

            return elem;    
        }

        function showLazyLoad () {
            $(".infiniteLoader-lazy-load").show('fast');
        }

        $(window).scroll(function(){
            if  (($(window).scrollTop() == $(document).height() - $(window).height())
            && !isMax){
                //loadTweets();
                if (loadCounter < maxAutoLoad) {
                    loadTweets();
                }
                else{
                    showLazyLoad();
                }
                
            }
        });

        $("#run-lazy-load").click(function() {
            loadTweets();
            $(".infiniteLoader-lazy-load").hide('fast');   
        });

    
    </script>
    
% include('base_footer.tpl')
