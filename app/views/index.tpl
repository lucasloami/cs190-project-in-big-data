<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Twitter Tracker</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="static/landing-page/css/base.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    

   <!-- <script src="static/js/jquery.min.js"></script> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- <script src="static/js/bootstrap.min.js"></script> -->
     <script src="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <script src="static/js/asterix-sdk-stable.js"></script>
    <!-- <script src="static/js/demo.js"></script> -->
    <script src="static/js/twitter-tracker.js"></script>



  </head>
  <body>
  
	<div class="jumbotron">
	  	<div class="container">
	  		<div>
	  			<h1 class="text-center">Twitter Tracker!</h1>
				<p class="text-center">Select the topic you want to track</p>
				<div>
					<p class="text-center">
						<a href="/dashboard" id="topic-obama">
							<img src="static/landing-page/img/obama.png" alt="Obama picture" class="img-circle highlight">
						</a>
					</p>
				</div>
	  		</div>
			
		</div>
	</div>
  </body>
</html>



